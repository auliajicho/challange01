const dataPenjualanNovel = [
    {
      idProduk : 'BOOK002421',
      namaProduk : 'Pulang-Pergi',
      penulis : 'Tere Liye',
      hargaBeli : 60000,
      hargaJual : 86000,
      totalTerjual : 150,
      sisaStok : 17,
    },
  	{
      idProduk : 'BOOK002351',
      namaProduk : 'Selamat Tinggal',
      penulis : 'Tere Liye',
      hargaBeli : 75000,
      hargaJual : 103000,
      totalTerjual : 171,
      sisaStok : 20,
    },
  	{
      idProduk : 'BOOK002941',
      namaProduk : 'Garis Waktu',
      penulis : 'Fiersa Besari',
      hargaBeli : 67000,
      hargaJual : 99000,
      totalTerjual : 213,
      sisaStok : 5,
    },
  	{
      idProduk : 'BOOK002941',
      namaProduk : 'Laskar Pelangi',
      penulis : 'Andrea Hirata',
      hargaBeli : 55000,
      hargaJual : 68000,
      totalTerjual : 20,
      sisaStok : 56,
    }
  ]

function getInfoPenjualan(dataPenjualanNovel){
  let totalKeuntungan=0;
  let modal =0;
  let presentasiKeuntungan=0;
  for (i=0;i<dataPenjualanNovel.length ; i++){
      totalKeuntungan +=dataPenjualanNovel[i].hargaJual*dataPenjualanNovel[i].totalTerjual
      modal += dataPenjualanNovel[i].hargaBeli*(dataPenjualanNovel[i].totalTerjual+dataPenjualanNovel[i].sisaStok)
      //presentasiKeuntungan += ((dataPenjualanNovel[i].hargaJual-dataPenjualanNovel[i].hargaBeli)/dataPenjualanNovel[i].hargaBeli)*100

  }
  presentasiKeuntungan = ((totalKeuntungan-modal)/modal)*100;

  const max = dataPenjualanNovel.reduce(function(prev,cur){
    return prev.totalTerjual> cur.totalTerjual ? prev : cur;
  })

  const format = totalKeuntungan.toString().split('').reverse().join('');
  const convert = format.match(/\d{1,3}/g);
  const rupiah = 'Rp '+ convert.join('.').split('').reverse().join('')

  const format1 = modal.toString().split('').reverse().join('');
  const convert1 = format1.match(/\d{1,3}/g);
  const rupiah1 = 'Rp '+ convert1.join('.').split('').reverse().join('')

  return {
     totalKeuntungan : rupiah ,
     modal : rupiah1,
     presentasiKeuntungan : presentasiKeuntungan,
     produkBukuTerlaris : max.namaProduk,
     penulisTerlaris : max.penulis
    }
}

console.log(getInfoPenjualan(dataPenjualanNovel))
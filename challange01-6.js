function getAngkaTerbesarKedua(dataAngka){
    if (!Array.isArray(dataAngka)){
      return "Error : This is not an Array"
    }
    else if (dataAngka.length<=1 ){
      return "Error : Please Input your Array Number"
    }
    dataAngka.sort((i,j) => {return i-j}).reverse();
    
    let angkaTerbesar = Math.max(...dataAngka);
    let totalAngkaTerbesar = dataAngka.filter((dataAngka) => dataAngka === angkaTerbesar).length
    
    let angkaTerbesarKedua = 0;
    
    return dataAngka[angkaTerbesarKedua + totalAngkaTerbesar]
  }
  const dataAngka = [9,4,7,7,4,3,2,2,8,10,11];
  
  console.log(getAngkaTerbesarKedua(dataAngka));
  console.log(getAngkaTerbesarKedua(1));
  console.log(getAngkaTerbesarKedua([]));
const dataPenjualPakAldi = [
    {
        namaProduct : 'Sepatu Futsal Nike Vapor Academy 8',
        hargaSatuan : 760000,
        kategori : "Sepatu Sport",
        totalTerjual : 90,
    },
    {
        namaProduct : 'Sepatu Warior Tristan Black Brown High',
        hargaSatuan : 960000,
        kategori : "Sepatu Sneaker",
        totalTerjual : 37,
    },
    {
        namaProduct : 'Sepatu Warior Tristan Maroon High',
        hargaSatuan : 360000,
        kategori : "Sepatu Sneaker",
        totalTerjual : 90,
    },
    {
        namaProduct : 'Sepatu Warior Rainbow Tosca Corduroy',
        hargaSatuan : 120000,
        kategori : "Sepatu Sneaker",
        totalTerjual : 90,
    }
]   

function hitungTotalPenjualan(dataPenjualan){
	let totalPenjumlahan = 0
  for (i=0;i<dataPenjualPakAldi.length ;i++){
    totalPenjumlahan += dataPenjualPakAldi[i].totalTerjual;
  }
  
  return totalPenjumlahan;
  
}

console.log(hitungTotalPenjualan(dataPenjualPakAldi))